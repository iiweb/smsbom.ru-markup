

/* Карусель бомб
--------------------------------*/
$('#bombs-carousel').jcarousel({
	'animation': {
		'duration':300
	}
});
function bombsCarouselScroll(id) {
	if (typeof(id)=='string') {
		switch (id) {
			case 'prev':
				$('#bombs-carousel').jcarousel('scroll', '-=1');
				break;
			case 'next':
				$('#bombs-carousel').jcarousel('scroll', '+=1');
				break;
		}
	}
	if (typeof(id)=='number') {
		$('#bombs-carousel').jcarousel('scroll', id-1);
	}
	$('.bombs-carousel-page-btn').removeClass('btn-large btn-info');
	$('#bombs-carousel-page-'+id).addClass('btn-large btn-info');
}
$('#bombs-carousel-left').click(function() {
	bombsCarouselScroll(1);
});
$('#bombs-carousel-right').click(function() {
	bombsCarouselScroll(2);
});
$('#bombs-carousel-page-1').click(function() {
	bombsCarouselScroll(1);
});
$('#bombs-carousel-page-2').click(function() {
	bombsCarouselScroll(2);
});


/* Карусель отзывов
--------------------------------*/
$('#comments-carousel').jcarousel({
	'animation': {
		'duration':300
	},
	'wrap': 'circular'
});
$('#comments-left').click(function() {
	$('#comments-carousel').jcarousel('scroll', '-=1');
});
$('#comments-right').click(function() {
	$('#comments-carousel').jcarousel('scroll', '+=1');
});


/* jScrollPane для контента отзыва
------------------------------------------*/
$('#comments .comment-text').jScrollPane();


/* Настройка модального окна
-------------------------------------*/
$('#modalWrapper').bind('click.myEvent', function(e) {
	var wnd = $('#modal');
	if (!$(e.target).closest(wnd).length) {
		wnd.modal('hide');
		//$(this).unbind('click.myEvent');
	}
});
/*
$('#modalWrapper').click(function(e){
	if ($)
	$('#modal').modal('hide');
});
*/
$('#modal').on('show', function() {
	$(this).parent().show();
}).on('shown', function () {
    var c = $(this).find('.jscrollpane');
    if (!c.hasClass('jspScrollable')) {
		c.jScrollPane();
	}
}).on('hidden', function() {
	$(this).parent().hide();
    $(this).data('modal').$element.removeData(); // Заставляет bootstrap перезагрузить содержимое модального окна
});


/* - Привязка окна к кнопкам
   - Выбор категории в зависимости от кнопки
   - Cсылки "подробнее"
-----------------------------------------------------*/
/* Любовь */
$('#solution-1 .btn-success').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smsform.html'
	}).on('shown', function(){
		$('#smsForm #sendCategory').val('love1');
	});
}).parents('.thumbnail').find('a.more-link').click(function(e){
	e.preventDefault();
	$('#modal').addClass('smsbomb-modal-solutions').modal({
		remote: '/content/ajax/love.html'
	}).on('hidden',function(){
		$(this).removeClass('smsbomb-modal-solutions');
	});
});

/* Поздравление */
$('#solution-2 .btn-success').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smsform.html'
	}).on('shown', function(){
		$('#smsForm #sendCategory').val('congrat1');
	})
}).parents('.thumbnail').find('a.more-link').click(function(e){
	e.preventDefault();
	$('#modal').addClass('smsbomb-modal-solutions').modal({
		remote: '/content/ajax/congrat.html'
	}).on('hidden',function(){
		$(this).removeClass('smsbomb-modal-solutions');
	});
});

/* Извинение */
$('#solution-3 .btn-success').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smsform.html'
	}).on('shown', function(){
		$('#smsForm #sendCategory').val('sorry1');
	})
}).parents('.thumbnail').find('a.more-link').click(function(e){
	e.preventDefault();
	$('#modal').addClass('smsbomb-modal-solutions').modal({
		remote: '/content/ajax/sorry.html'
	}).on('hidden',function(){
		$(this).removeClass('smsbomb-modal-solutions');
	});
});

/* Розыгрыш */
$('#solution-4 .btn-success').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smsform.html'
	}).on('shown', function(){
		$('#smsForm #sendCategory').val('prank1');
	})
}).parents('.thumbnail').find('a.more-link').click(function(e){
	e.preventDefault();
	$('#modal').addClass('smsbomb-modal-solutions').modal({
		remote: '/content/ajax/prank.html'
	}).on('hidden',function(){
		$(this).removeClass('smsbomb-modal-solutions');
	});
});

/* Тестовая рассылка */
$('#service-test-btn').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smstestform.html'
	});
})


/* Меню личного кабинета
---------------------------------------*/
$('#user-action-1').click(function(e){
	e.preventDefault();
	$('#modal').modal({
		remote: '/content/ajax/smsform.html'
	});
});


/* Кнопка "Вход"
----------------------------------------*/
$('#btn-user').click(function(e){
	e.preventDefault();
	$('#modal').addClass('account-modal').modal({
		remote: '/content/ajax/account.html'
	}).on('hidden',function(){
		$(this).removeClass('account-modal');
	});
});


/* Кнопки в личном кабинете
--------------------------------------------*/
$('#profileNameEdit').tooltip();
$('.profile-history-more').tooltip();


/* Слайд-анимация для подробной инфы по
   рассылке
---------------------------------------------*/
var animationSpeed = 400;
$('.profile-history-more').click(function(e){
	e.preventDefault();
	var sendId = $(this).parents('tr').attr('rel');
	$('#profile-history-details').append($('<div></div>').attr('id','profile-loading').css({background:'url("../img/loading.gif") 50% 50% no-repeat',height:'327px'}));
	$('#profile-history-clip-container').animate({'margin-left':'-654px'},animationSpeed,function(){
		$('#profile-history-details-container').hide();
		//window.setTimeout(function(){
			$('#profile-history-details-container').load('/content/ajax/history-detail/'+sendId+'.html',function(){
				$('#profile-loading').fadeOut('fast').remove();
				$(this).fadeIn();
			});
		//},1000);
	});
});
$('#profile-history-details-back').click(function(e){
	e.preventDefault();
	$('#profile-history-clip-container').animate({'margin-left':'0'},animationSpeed,function(){
		$('#profile-history-details-container').empty();
	});
});


/**/
//$('#collapse1-link').tooltip({placement:'left'});
$('#video-container a').click(function(e){
	e.preventDefault();
	$(this).parent().html('<iframe width="625" height="352" src="http://www.youtube.com/embed/CMnAm1ZFLEo?rel=0&wmode=opaque" frameborder="0" allowfullscreen></iframe>');
});